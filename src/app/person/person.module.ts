import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AllComponent } from './pages/all/all.component';
import { CreateComponent } from './pages/create/create.component';
import { UpdateComponent } from './pages/update/update.component';
import { MatTableModule } from '@angular/material/table';



@NgModule({
  declarations: [UpdateComponent, CreateComponent, AllComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: `all`,
        component: AllComponent
      },
      {
        path: `create`,
        component: CreateComponent
      },
      {
        path: `update`,
        component: UpdateComponent
      }
    ]),
    MatTableModule
  ]
})
export class PersonModule { }
