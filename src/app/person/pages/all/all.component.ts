import { Component, OnInit } from '@angular/core';
import { PersonService } from '../../service/person.service';
import { Person } from '../../models/Person';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.scss']
})
export class AllComponent implements OnInit {

  persons: any = null;
  displayedColumns: string[] = ['id', 'name', 'birthdate'];

  constructor(private personService: PersonService) { }

  ngOnInit() {
    const result = this.personService.getAll().subscribe({
      next: (value) => {
        console.log('result: ', value);
        this.persons = value;
      }
    });
  }

}
